def search_vowels(phrase: str) -> set:
    """Returns vowels found in the specified phrase"""
    vowels = set('aeiou')
    return vowels.intersection(set(phrase))


print(search_vowels('hello'))
print(help(search_vowels))


def search_letters(phrase: str, letters: str = 'aeiou') -> set:
    """Returns the set of letters from 'letters', found in the specified phrase """
    return set(letters).intersection(set(phrase))


print(help(search_letters))
print(search_letters('hello'))
print(search_letters(letters='xyz', phrase='galaxy'))


def double(arg):
    print('Before: ', arg)
    arg = arg * 2
    print('After: ', arg)


print(double("hello"))


def change(arg):
    print('Before: ', arg)
    arg.append('More data')
    print('After: ', arg)


print(change([1, 2, 3]))
